Source: libgnatcoll-db
Priority: optional
Section: libdevel
Maintainer: Nicolas Boulenguez <nicolas@debian.org>
Build-Depends:
 debhelper-compat (= 13),
Build-Depends-Arch:
 dh-ada-library (>= 9.1),
 dh-sequence-ada-library,
 gnat,
 gnat-14,
 gprbuild (>= 2025.0.0-2),
 libgnatcoll-dev,
 libgnatcoll-iconv-dev,
 libpq-dev,
 libsqlite3-dev,
Build-Depends-Indep:
 dh-sequence-sphinxdoc,
# Sphinx >= 1.6 uses the latexmk driver:
 latexmk,
 python3-sphinx,
# docs/conf.py
 python3-sphinx-rtd-theme,
 sphinx-common,
 tex-gyre,
 texlive-fonts-recommended,
 texlive-latex-extra,
Homepage: https://github.com/AdaCore/gnatcoll-db
Standards-Version: 4.7.1
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/debian/libgnatcoll-db
Vcs-Git: https://salsa.debian.org/debian/libgnatcoll-db.git

######################################################################

Package: libgnatcoll-sqlite-dev
Breaks: libgnatcoll-sqlite17-dev, libgnatcoll-sqlite18-dev,
  libgnatcoll-sqlite19-dev, libgnatcoll-sqlite20-dev, libgnatcoll-sqlite21-dev
Replaces: libgnatcoll-sqlite17-dev, libgnatcoll-sqlite18-dev,
 libgnatcoll-sqlite19-dev, libgnatcoll-sqlite20-dev, libgnatcoll-sqlite21-dev
Provides: ${ada:Provides}
Architecture: any
Depends: libsqlite3-dev, ${misc:Depends}, ${ada:Depends}
Suggests: libgnatcoll-db-bin, libgnatcoll-db-doc,
Description: Ada library accessing SQLite databases
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the static library and Ada specifications
 dedicated to SQLite databases manipulation.

Package: libgnatcoll-sqlite21
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada library accessing SQLite databases (runtime)
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the runtime shared library for the SQLite
 backend.


######################################################################

Package: libgnatcoll-db-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Built-Using: ${sphinxdoc:Built-Using}
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Suggests: gnat
Description: Ada library accessing SQL databases (documentation)
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the documentation in text, PDF and HTML.

Package: libgnatcoll-db-bin
Section: devel
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libgnatcoll-db-doc,
# libgnatcoll-sqlite-bin <= 18.4 was providing gnatinspect.
Breaks: libgnatcoll-sqlite-bin
Replaces: libgnatcoll-sqlite-bin
Description: Ada library accessing SQL databases (tools)
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains command-line tools.
 * gnatcoll_db2ada generates Ada sources depending on
   libngatcoll-sqlite or libgnatcoll-postgres.
 * gnatinspect runs in a source tree and generate a database of
   cross-references.

######################################################################

Package: libgnatcoll-sql-dev
Breaks: libgnatcoll-sql1-dev, libgnatcoll-sql2-dev, libgnatcoll-sql3-dev,
 libgnatcoll-sql4-dev, libgnatcoll-sql5-dev
Replaces: libgnatcoll-sql1-dev, libgnatcoll-sql2-dev, libgnatcoll-sql3-dev,
 libgnatcoll-sql4-dev, libgnatcoll-sql5-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Suggests: libgnatcoll-db-bin, libgnatcoll-db-doc,
Description: Ada library accessing SQL databases
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the static library and Ada specifications of
 the backend-independant part.

Package: libgnatcoll-sql4
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada library accessing SQL databases (runtime)
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the runtime shared library of the
 backend-independant part.

######################################################################

Package: libgnatcoll-xref-dev
Breaks: libgnatcoll-xref18-dev, libgnatcoll-xref19-dev, libgnatcoll-xref20-dev,
 libgnatcoll-xref21-dev, libgnatcoll-xref22-dev
Replaces: libgnatcoll-xref18-dev, libgnatcoll-xref19-dev, libgnatcoll-xref20-dev,
 libgnatcoll-xref21-dev, libgnatcoll-xref22-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Suggests: libgnatcoll-db-bin, libgnatcoll-db-doc,
Description: Ada library for manipulation of Ada cross references
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the static library and Ada specifications for
 the XRef module, which builds a database of cross references from a
 source tree.

Package: libgnatcoll-xref21
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada library for manipulation of Ada cross references (runtime)
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the runtime shared library for the XRef module,
 which builds a database of cross references from a source tree.

######################################################################

Package: libgnatcoll-postgres-dev
Breaks: libgnatcoll-postgres1-dev, libgnatcoll-postgres2-dev,
 libgnatcoll-postgres3-dev
Replaces: libgnatcoll-postgres1-dev, libgnatcoll-postgres2-dev,
 libgnatcoll-postgres3-dev
Provides: ${ada:Provides}
Architecture: any
Depends: ${misc:Depends}, ${ada:Depends}
Suggests: libgnatcoll-db-bin, libgnatcoll-db-doc,
Description: Ada library accessing PostGreSQL databases
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the static library and Ada specifications
 dedicated to PostGreSQL databases manipulation.

Package: libgnatcoll-postgres2
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Ada library accessing PostGreSQL databases (runtime)
 The gnatcoll-db library allows Ada programs to send SQL queries to
 SQLite or PostGreSQL databases.
 .
 This package contains the runtime shared library for the PostGreSQL
 backend.
